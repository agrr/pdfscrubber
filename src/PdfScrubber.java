import java.io.*;

import com.itextpdf.text.pdf.*;

public class PdfScrubber {
	
	static String getBasename(String filename)
	{
		int dot = filename.lastIndexOf('.');
		String base = (dot == -1) ? filename: filename.substring(0, dot);
		
		return base;
	}

	public static void main(String[] args) throws Exception {
		PdfReader reader = null;
		PdfReader.debugmode = true;
		String EVIL_LINK = "http://www.it-ebooks.info/";
		String EVIL_TEXT = "www.it-ebooks.info";
		
		if (args.length == 0)
		{
			System.err.println("Usage: java -jar pdf-scrubber.jar [INPUT_PDF]");
			System.exit(0);
		}
		String outname = getBasename(args[0])+"_scrubbed.pdf"; 
		FileOutputStream os = new FileOutputStream(outname);
		
		reader = new PdfReader(args[0]);
		PdfStamper stamper = new PdfStamper(reader, os);;
	
		//Iterate over all the PDF objects
		for (int i=1; i != reader.getXrefSize(); i++)
		{
			PdfObject obj = reader.getPdfObject(i);
			PdfDictionary dict;
			if (obj == null)
			{
				continue;
			}
			//Remove the text at every page footer
			if (obj.isStream())
			{
				PdfStream s = (PdfStream)obj;
				if (s.get(new PdfName("LC")) != null)
				{
//					System.err.println("Found LC stream!");
					byte[] hello = PdfReader.getStreamBytes((PRStream)s);
					String tmp_content = new String(hello);
					tmp_content = tmp_content.replace(EVIL_TEXT, "");
					((PRStream)s).setData(tmp_content.getBytes());
					
				}
				
			}
			//Remove the link annotation
			else if (obj.isDictionary())
			{
				dict = (PdfDictionary)obj;
				if (dict.get(PdfName.TYPE) == PdfName.ANNOT && dict.get(PdfName.SUBTYPE) == PdfName.LINK)
				{
				        PdfObject target_obj = dict.get(PdfName.A);
					if (!target_obj.isDictionary())
						continue;
					PdfDictionary uri = (PdfDictionary)target_obj;
					if (uri != null && uri.get(PdfName.URI) != null)
					{
						if (uri.get(PdfName.URI).toString().equals(EVIL_LINK))
						{
							dict.put(PdfName.SUBTYPE, null);
										
						}

					}

				}
				
			}
				
		}

		stamper.close();
		System.out.println("Scrubbed file written to: "+outname);
	}
}
